const { ModuleFederationPlugin } = require("webpack").container;
const path = require("path");

module.exports = {
  entry: "./src/index",
  devServer: {
    contentBase: __dirname,
    publicPath: '/',
    compress: true,
    port: 3013
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "integration",
      library: { type: "var", name: "integration" },
      remotes: {
        randomPic: "randomPic",
      },
      // shared must be defined in consumer as well as in the component itself
      // otherwise chunk is loaded twice from consumer and component
      shared: ["@material/mwc-button"]
    })
  ],
};
