# random-pic-component-integration

The counterpart of the [random-pic-component](https://gitlab.com/josros/random-pic-component).
Here, the [random-pic-component](https://gitlab.com/josros/random-pic-component) is integrated using [webpack 5 module federation](https://webpack.js.org/concepts/module-federation/).

Have a look a the [demo page](https://josros.gitlab.io/random-pic-component-integration/).

## Executive summary

### Local execution

```bash
# install dependencies
npm i

# run and view integration locally
npm run start
open http://localhost:3013/

```