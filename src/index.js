import("@material/mwc-button")
// loads component using module federation
import("randomPic/component").then(() => {
    // <random-pic update-freq-ms="15000" image-length-px="300"></random-pic>
    const element = document.querySelector("#component-container");
    const randomPic = document.createElement('random-pic');
    randomPic.setAttribute('update-freq-ms', '15000');
    randomPic.setAttribute('image-length-px', '300');
    element.appendChild(randomPic);
})